class Potepan::ProductsController < ApplicationController
  MAX_PRODUCTS_COUNT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.collect_related_products.limit(MAX_PRODUCTS_COUNT).includes(master: [:default_price, :images])
  end
end
