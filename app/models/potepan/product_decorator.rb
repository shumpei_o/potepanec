Spree::Product.class_eval do
  def collect_related_products
    Spree::Product.joins(:taxons).
      where(spree_products_taxons: { taxon_id: taxons.ids }).
      where.not(id: id).
      order(:id).
      distinct
  end
end
