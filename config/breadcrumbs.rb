crumb :home do
  link "HOME", potepan_path
end

crumb :shop do
  link "SHOP", "#"
  parent :home
end

crumb :categories do |taxon|
  link "#{taxon.name}"
  parent :shop
end

crumb :products do |product|
  link "#{product.name}"
  parent :home
end
