require 'rails_helper'

RSpec.describe "Categories", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon)    { create(:taxon, name: "Ruby", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:product)  { create(:product, name: "rubyBAG", price: 20, taxons: [taxon]) }
  let(:image)    { create(:image) }
  let!(:other_taxon)    { create(:taxon, name: "Rails", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:other_products) { create(:product, name: "railstote", price: 50, taxons: [other_taxon]) }

  before do
    product.master.images = [image]
    visit potepan_category_path(taxon.id)
  end

  describe "categoryページ表示のテスト" do
    it "カテゴリーに紐づいたデータが正しく表示されていること" do
      expect(page).to have_selector ".page-title", text: "Ruby"
      expect(page).to have_selector ".breadcrumb", text: "Ruby"
      expect(page).to have_selector ".productCaption", text: "rubyBAG"
      expect(page).to have_selector ".productCaption", text: "20"
    end

    it "他のカテゴリーに紐づいたデータが表示されていないこと" do
      expect(page).not_to have_selector ".page-title", text: "Rails"
      expect(page).not_to have_selector ".breadcrumb", text: "Rails"
      expect(page).not_to have_selector ".productCaption", text: "railstote"
      expect(page).not_to have_selector ".productCaption", text: "50"
    end

    it ".productBoxが商品の数だけ表示されていること" do
      expect(page).to have_selector('.productBox', count: taxon.all_products.count)
    end

    it "商品のリンクが正しく実装されていること" do
      click_link "rubyBAG"
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end

  describe "商品画像のテスト" do
    it "商品画像が正しく表示されていること" do
      expect(page).to have_selector("img[alt=商品画像-#{product.id}]")
    end

    it "商品画像のリンクが正しく実装されていること" do
      click_link "商品画像-#{product.id}"
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end

  describe "サイドバーのテスト" do
    it "サイドバーが正しく表示されていること" do
      within ".side-nav" do
        expect(page).to have_content "Categories"
        expect(page).to have_content "Ruby (1)"
        expect(page).to have_content "Rails (1)"
      end
    end

    it "javascript:;がtaxonomyの数だけ表示されていること" do
      expect(page).to have_link nil, href: "javascript:;", count: Spree::Taxonomy.all.count
    end

    it "商品カテゴリーのリンクが正しく実装されていること" do
      within ".side-nav" do
        click_link "Rails"
      end
      expect(current_path).to eq potepan_category_path(other_taxon.id)
      expect(page).to have_selector ".page-title", text: "Rails"
      expect(page).to have_selector ".breadcrumb", text: "Rails"
      expect(page).to have_selector ".productCaption", text: "railstote"
      expect(page).to have_selector ".productCaption", text: "50"
    end
  end
end
