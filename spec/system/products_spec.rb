require 'rails_helper'

RSpec.describe "Products", type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, name: "Ruby", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:product)  { create(:product, name: "potepanBAG", price: 20, description: "potepan_new_BAG", taxons: [taxon]) }
  let(:image)    { create(:image) }

  describe "商品詳細ページ表示のテスト" do
    before do
      product.master.images = [image]
      visit potepan_product_path(product.id)
    end

    it "商品詳細ページが正しく表示されていること" do
      expect(page).to have_title "potepanBAG - BIGBAG Store"
      expect(page).to have_selector ".page-title", text: "potepanBAG"
      expect(page).to have_selector ".breadcrumb", text: "potepanBAG"
      expect(page).to have_selector ".media-body", text: "potepanBAG"
      expect(page).to have_selector ".media-body", text: "$20"
      expect(page).to have_selector ".media-body", text: "potepan_new_BAG"
      expect(page).to have_selector("img[alt=商品画像-0-large]")
      expect(page).to have_selector("img[alt=商品画像-0-small]")
    end
  end

  describe "トップページリンクのテスト" do
    before do
      visit potepan_product_path(product.id)
    end

    it "ロゴ画像のリンクが正しく設定されていること" do
      click_link "ロゴ画像"
      expect(current_path).to eq potepan_path
    end

    it "ヘッダー内のリンクが正しく設定されていること" do
      within '.navbar-right' do
        click_link "HOME"
      end
      expect(current_path).to eq potepan_path
    end

    it "LIGHT-SECTION内のリンクが正しく設定されていること" do
      within '.breadcrumb' do
        click_link "HOME"
      end
      expect(current_path).to eq potepan_path
    end
  end

  describe "一覧ページへ戻るリンクのテスト" do
    context "productにtaxonが紐づいているとき" do
      it "リンクが正しく設定されていること" do
        visit potepan_product_path(product.id)
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
        expect(page).to have_selector ".page-title", text: "Ruby"
      end
    end

    context "productにtaxonが紐づいてないとき" do
      let(:other_product) { create(:product) }

      it "リンクが表示されないこと" do
        visit potepan_product_path(other_product.id)
        expect(current_path).to eq potepan_product_path(other_product.id)
        expect(page).not_to have_link "一覧ページへ戻る"
      end
    end
  end

  describe "関連商品のテスト" do
    let(:no_related_taxon)    { create(:taxon, name: "Rails", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:no_related_product) { create(:product, name: "railsCLOCK", price: 100, taxons: [no_related_taxon]) }
    let(:related_product)     { create(:product, name: "rubyT-SHIRT", price: 40, taxons: [taxon]) }

    before do
      related_product.master.images = [image]
      visit potepan_product_path(product.id)
    end

    it "関連商品が正しく表示されること" do
      within ".productBox" do
        expect(page).to have_selector "h5", text: "rubyT-SHIRT"
        expect(page).to have_selector "h3", text: "$40"
      end
      expect(page).to have_selector("img[alt=関連商品画像-0]")
    end

    it "リンクが正しく設定されていること" do
      click_link "関連商品画像-0"
      expect(current_path).to eq potepan_product_path(related_product.id)
      expect(page).to have_selector ".page-title", text: "rubyT-SHIRT"
    end

    it "無関連の商品が表示されていないこと" do
      expect(current_path).to eq potepan_product_path(product.id)
      expect(page).not_to have_content "railsCLOCK"
    end

    context "関連商品が5個以上ある時" do
      let!(:related_products1) { create_list(:product, 5, taxons: [taxon]) }

      it ".productBoxが4個表示されていること" do
        visit potepan_product_path(product.id)
        expect(page).to have_selector('.productBox', count: 4)
      end
    end

    context "関連商品が4個以下の時" do
      let!(:related_products2) { create_list(:product, 4, taxons: [taxon]) }

      it ".productBoxが関連商品の数だけ表示されていること" do
        visit potepan_product_path(product.id)
        expect(page).to have_selector('.productBox', count: 4)
      end
    end
  end
end
