require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "collect_related_productsメソッドのテスト" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, name: "Ruby", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:product)  { create(:product, name: "rubyBAG", taxons: [taxon]) }
    let(:no_related_taxon)    { create(:taxon, name: "Rails", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:no_related_product) { create(:product, name: "railsCLOCK", taxons: [no_related_taxon]) }
    let(:products) { product.collect_related_products }

    it "無関連の商品が取得されていないこと" do
      expect(products).not_to include "railsCLOCK"
    end

    context "関連商品が存在するとき" do
      let!(:related_product1) { create(:product, name: "rubyT-SHIRT", taxons: [taxon]) }
      let!(:related_product2) { create(:product, name: "rubyTOTE", taxons: [taxon]) }
      let!(:related_product3) { create(:product, name: "rubyCLOCK", taxons: [taxon]) }
      let!(:related_product4) { create(:product, name: "rubyMUG", taxons: [taxon]) }

      it "全ての関連商品を取得すること" do
        expect(products[0].name).to eq "rubyT-SHIRT"
        expect(products[1].name).to eq "rubyTOTE"
        expect(products[2].name).to eq "rubyCLOCK"
        expect(products[3].name).to eq "rubyMUG"
      end
    end

    context "関連商品が存在しないとき" do
      it "関連商品を取得しないこと" do
        expect(products.count).to eq 0
      end
    end
  end
end
