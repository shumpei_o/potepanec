require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan/categories/:id" do
    let(:taxonomy) { create(:taxonomy, name: "Categories") }
    let(:taxon)    { create(:taxon, name: "Ruby", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, name: "rubyBAG", price: 20, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "200レスポンスが返ってくること" do
      expect(response).to have_http_status(:success)
    end

    it "taxonomyの名前が含まれていること" do
      expect(response.body).to include "Categories"
    end

    it "taxonの名前が含まれていること" do
      expect(response.body).to include "Ruby"
    end

    it "taxonに紐づいた商品名が含まれていること" do
      expect(response.body).to include "rubyBAG"
    end
  end
end
