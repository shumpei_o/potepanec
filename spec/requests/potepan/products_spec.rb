require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET /potepan/products/:id" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, name: "Ruby", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:product)  { create(:product, name: "Ruby on Rails Bag", taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "200レスポンスが返ってくること" do
      expect(response).to have_http_status(200)
    end

    it "プロダクト名が含まれていること" do
      expect(response.body).to include "Ruby on Rails Bag"
    end

    context "関連商品が5個以上のとき" do
      let!(:related_product1) { create(:product, name: "rubyT-SHIRT", taxons: [taxon]) }
      let!(:related_product2) { create(:product, name: "rubyTOTE", taxons: [taxon]) }
      let!(:related_product3) { create(:product, name: "rubyCLOCK", taxons: [taxon]) }
      let!(:related_product4) { create(:product, name: "rubyMUG", taxons: [taxon]) }
      let!(:related_product5) { create(:product, name: "rubyRING", taxons: [taxon]) }

      before do
        get potepan_product_path(product.id)
      end

      it "4個の関連商品名までが含まれていること" do
        expect(response.body).to include "rubyT-SHIRT"
        expect(response.body).to include "rubyTOTE"
        expect(response.body).to include "rubyCLOCK"
        expect(response.body).to include "rubyMUG"
      end

      it "5個目の関連商品名が含まれていないこと" do
        expect(response.body).not_to include "rubyRING"
      end
    end

    context "関連商品が4個以下のとき" do
      let!(:related_product1) { create(:product, name: "rubyT-SHIRT", taxons: [taxon]) }
      let!(:related_product2) { create(:product, name: "rubyTOTE", taxons: [taxon]) }
      let!(:related_product3) { create(:product, name: "rubyCLOCK", taxons: [taxon]) }
      let!(:related_product4) { create(:product, name: "rubyMUG", taxons: [taxon]) }

      it "全ての関連商品名が含まれていること" do
        get potepan_product_path(product.id)
        expect(response.body).to include "rubyT-SHIRT"
        expect(response.body).to include "rubyTOTE"
        expect(response.body).to include "rubyMUG"
        expect(response.body).to include "rubyCLOCK"
      end
    end
  end
end
