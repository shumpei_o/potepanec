require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    context "引数がないとき" do
      it "BASE_TITLEのみ表示されること" do
        expect(helper.full_title).to eq "BIGBAG Store"
      end
    end

    context "page_titleがあるとき" do
      it "page_title - BASE_TITLEが表示されること" do
        expect(helper.full_title(page_title: "potepantote")).to eq "potepantote - BIGBAG Store"
      end
    end
  end
end
